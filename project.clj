(defproject slackbot-clj "0.1.0"
  :description "TODO"
  :url "TODO"
  :license {:name "TODO: Choose a license"
            :url "http://choosealicense.com/"}
  :dependencies [[clj-time "0.14.0"]
                 #_[com.fzakaria/slf4j-timbre "0.3.7"]
                 [com.stuartsierra/component "0.3.2"]
                 [com.taoensso/timbre "4.10.0"]
                 [compojure "1.6.0"]
                 [clj-http "3.7.0"]
                 [environ "1.1.0"]
                 [hickory "0.7.1"]
                 [http-kit "2.3.0-alpha2"]
                 [jarohen/chime "0.2.2"]
                 [org.clojure/clojure "1.8.0"]
                 [ring "1.6.2"]
                 [ring/ring-defaults "0.3.1"]
                 [ring/ring-json "0.4.0"]]
  :main ^:skip-aot slackbot-clj.core
  :profiles {:dev {:dependencies [[com.stuartsierra/component.repl "0.2.0"]
                                  [org.clojure/tools.namespace "0.2.11"]
                                  [ring/ring-mock "0.3.0"]]
                   :source-paths ["dev"]
                   :repl-options {:init-ns user}}
             :uberjar {:aot :all}})
