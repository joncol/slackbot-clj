(ns slackbot-clj.lunch-times-test
  (:require [clojure.test :refer [deftest is testing]]
            [slackbot-clj.lunch-times :refer [lunch-times]]))

(deftest test-lunch-times
  (let [ts (take 1000 lunch-times)]
    (testing "hour is not less than 9"
      (is (every? #(not (pos? (compare 9 (.getHourOfDay %)))) ts)))
    (testing "hour is not greater than 12"
      (is (every? #(not (neg? (compare 12 (.getHourOfDay %)))) ts)))
    (testing "minute of hour is 0 or 30"
      (is (every? (fn [t] (-> t .getMinuteOfHour #{0 30})) ts)))))
