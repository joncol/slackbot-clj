(ns slackbot-clj.slackbot-system
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [slackbot-clj.app-server-component :refer [new-app-server]]
            [slackbot-clj.download-menus-component
             :refer [new-download-menus-component]]))

(defn new-system [config]
  (component/system-map
   :download-menus-component (new-download-menus-component)
   :app-server (component/using (new-app-server config)
                                [:download-menus-component])))
