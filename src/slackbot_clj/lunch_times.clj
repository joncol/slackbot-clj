(ns slackbot-clj.lunch-times
  (:require [clj-time.core :as t]
            [clj-time.local :as l]
            [clj-time.periodic :refer [periodic-seq]])
  (:import [org.joda.time DateTimeConstants DateTimeZone]))

(def lunch-times
  (->> (periodic-seq (.. (l/local-now)
                         (withZone (DateTimeZone/forID "Europe/Stockholm"))
                         (withTime 9 0 0 0))
                     (-> 30 t/minutes))
       (remove (comp #{DateTimeConstants/SATURDAY DateTimeConstants/SUNDAY}
                     #(.getDayOfWeek %)))
       (remove (comp #(> 9 %) #(.getHourOfDay %)))
       (partition-by #(.getDayOfWeek %))
       (map #(take-while (fn [t] (< (.getHourOfDay t) 13)) %))
       flatten))
