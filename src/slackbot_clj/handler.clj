(ns slackbot-clj.handler
  (:require [clj-time.local :as l]
            [clj-time.format :as f]
            [clojure.string :as str]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response :as resp]
            [slackbot-clj.download-menus-component :refer [restaurants]]))

(def ^:private status
  {:status 200
   :body {:system "slackbot-clj"
          :status "ok"}})

(defn- menu-str [name menu]
  (str "\n" name "\n"
       (apply str (repeat (count name) "-")) "\n"
       (str/join "\n" menu)))

(defn- timestamp-str [time]
  (f/unparse (:mysql l/*local-formatters*) time))

(defn- footer-str [time]
  (str "---\n" "Data generated " (timestamp-str time)
       " with 💜 using Clojure\n"))

(defn- regex? [x]
  (= (type x) java.util.regex.Pattern))

(defn- vec-matches? [vec x]
  (some #(cond
           (string? %) (= x %)
           (regex? %) (re-find % x))
        vec))

(defn- alias->key [restaurants alias]
  (->> restaurants
       keys
       (filter #(vec-matches? % alias))
       first))

(defn- alias->name [restaurants alias]
  (get restaurants (alias->key restaurants alias)))

(defn- lunch-resp [download-menus-component restaurant-aliases]
  (let [menu @(:menu download-menus-component)]
    (if (seq restaurant-aliases)
      (let [keys (map (partial alias->key restaurants) restaurant-aliases)]
        (str "```"
             (reduce (fn [acc key]
                       (let [restaurant-id (first key)]
                         (if (contains? menu restaurant-id)
                           (str acc
                                (menu-str (get restaurants key)
                                          (get menu restaurant-id))
                                "\n\n")
                           acc)))
                     "" keys)
             (footer-str @(:timestamp download-menus-component))
             "```"))
      (str "```"
           (str/join "\n\n"
                     (map #(menu-str
                            (alias->name restaurants (key %))
                            (val %))
                          menu))
           "\n\n"
           (footer-str @(:timestamp download-menus-component))
           "```"))))

(defn app-routes-wrapper [download-menus-component]
  (defroutes app-routes
    (GET "/status" _ status)
    (GET "/" {{command :command restaurant-str :text
               :or {restaurant-str ""}} :params}
      (-> (resp/response
           (let [cmd                (str/replace command #"/" "")
                 restaurant-aliases (->> (str/split restaurant-str #"\s+")
                                         (remove empty?))]
             (condp contains? cmd
               #{"lunch" "lp"} (lunch-resp download-menus-component
                                           restaurant-aliases)
               (str "Unknown command: " cmd))))
          (resp/content-type "text/plain")))
    (route/not-found "Not found")))

(defn app [download-menus-component]
  (-> (app-routes-wrapper download-menus-component)
      (wrap-json-body {:keywords? true :bigdecimals? true})
      (wrap-json-response {:pretty true})
      (wrap-defaults site-defaults)))
