(ns slackbot-clj.parser
  (:require [clj-http.client :as client]
            [clojure.string :as str]
            [hickory.core :as h]
            [hickory.select :as s]))

(defn download-menus-page-body [url]
  (-> (client/get url) :body))

(defn get-menu [page-body restaurant-id]
  (->> (s/select (s/child (s/and (s/class "dagenslunchruta")
                                 (s/id restaurant-id))
                          (s/tag :p)) (-> page-body h/parse h/as-hickory))
       first
       :content
       (filter string?)
       (map str/trim)
       (remove empty?)
       (remove #(str/includes? % "Med reservation för ändring"))))
