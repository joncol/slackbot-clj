(ns slackbot-clj.download-menus-component
  (:require [clj-time.local :as l]
            [clojure.core.async :as async]
            [com.stuartsierra.component :as component]
            [slackbot-clj.lunch-times :refer [lunch-times]]
            [slackbot-clj.parser :refer [download-menus-page-body get-menu]]
            [slackbot-clj.scheduler :refer [schedule-job]]
            [slackbot-clj.time-util :refer [weekday-abbr]]
            [taoensso.timbre :as timbre]))

(defn- url []
  (str "https://www.avenyn.se/dagens-lunch-" (weekday-abbr)))

;; First key word must be string, not regex.
(def restaurants
  {["bryggeriet" "br" "brygg"]                      "🏠 Bryggeriet"
   ["joe-farellis" "jf" #"^fa(relli|rre)s?$" "joe"] "🚬 Joe Farelli's"
   ["la-gondola-trattoria" "lg" #"^gondol(a|en)?$"] "🙄 La Gondola Trattoria"
   ["ruby-bar" "ruby" "rb"]                         "💎 Ruby"
   ["sjobaren" "sb" "sjöbaren"]                     "🐟 Sjöbaren"
   ["olstugan-tullen" #"(ö|o)t" "tullen"]           "🍺 Ölstugan Tullen"})

(defn- download-menu
  "Downloads the menu for a restaurant. If no restaurant-id is supplied, this
  function returns the menu for all restaurants."
  ([]
   (let [body (download-menus-page-body (url))
         keys (map first (keys restaurants))]
     (zipmap keys (pmap #(get-menu body %) keys))))

  ([restaurant-id]
   (let [body (download-menus-page-body (url))]
     (get-menu body restaurant-id))))

(defrecord DownloadMenusComponent [stop-ch menu timestamp]
  component/Lifecycle

  (start [component]
    (if (:stop-ch component)
      component
      (do (timbre/info "Starting DownloadMenusComponent")
          (let [stop-ch (async/chan)
                new-component (assoc component
                                     :stop-ch stop-ch
                                     :menu (atom (download-menu))
                                     :timestamp (atom (l/local-now)))]
            (schedule-job lunch-times
                          (fn []
                            (reset! (:menu new-component) (download-menu))
                            (reset! (:timestamp new-component) (l/local-now)))
                          stop-ch)
            new-component))))

  (stop [component]
    (if-let [stop-ch (:stop-ch component)]
      (do
        (timbre/info "Stopping DownloadMenusComponent")
        (async/close! stop-ch)
        (assoc component
               :stop-ch nil
               :menu nil
               :timestamp nil))
      component)))

(defn new-download-menus-component []
  (map->DownloadMenusComponent {}))
