(ns slackbot-clj.scheduler
  (:require [chime :refer [chime-ch]]
            [clojure.core.async :as async :refer [<! <!! go-loop]]))

(defn schedule-job [times job-fn stop-ch]
  (let [chimes (chime-ch times {:ch stop-ch})]
    (go-loop []
      (when-let [time (<! chimes)]
        (job-fn)
        (recur)))))
