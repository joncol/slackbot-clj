(ns slackbot-clj.time-util
  (:require [clj-time.core :as t]
            [clj-time.local :as l]))

(defn day-of-week->abbr [n]
  (case n
    1 "man"
    2 "tis"
    3 "ons"
    4 "tor"
    5 "fre"
    "fre"))

(defn weekday-abbr []
  (-> (l/local-now) t/day-of-week day-of-week->abbr))
