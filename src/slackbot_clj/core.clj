(ns slackbot-clj.core
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [slackbot-clj.config :refer [config]]
            [slackbot-clj.slackbot-system :refer [new-system]]
            [taoensso.timbre :as timbre]))

(defn -main [& args]
  (timbre/set-level! :info)
  (timbre/info "Starting slackbot-clj")
  (component/start-system (new-system config)))
