(ns slackbot-clj.config (:require [environ.core :refer [env]]))

(def config {:web-server {:port (or (env :port) 3000)}})
