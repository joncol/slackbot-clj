(ns slackbot-clj.app-server-component
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [ring.adapter.jetty :as jetty]
            [slackbot-clj.handler :refer [app]]
            [taoensso.timbre :as timbre]))

(defrecord AppServer [port server download-menus-component]
  component/Lifecycle

  (start [component]
    (if (:server component)
      component
      (do
        (timbre/info "Starting AppServer")
        (let [server (jetty/run-jetty (app download-menus-component)
                                      {:port port :join? false})]
          (assoc component :server server)))))

  (stop [component]
    (if-let [server (:server component)]
      (do (timbre/info "Stopping AppServer")
          (.stop server)
          (.join server)
          (assoc component :server nil))
      component)))

(defn new-app-server [config]
  (map->AppServer {:port (get-in config [:web-server :port])}))
