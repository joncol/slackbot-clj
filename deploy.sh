#!/usr/bin/env bash

app_name="slackbot-clj"
version="0.1.0"
app=$app_name-$version
jar=$app-standalone.jar

lein uberjar && \
    ssh -t jcodev.eu "sudo mkdir -p /var/www/$app_name/app ;and sudo chown -R www-data.www-data /var/www/$app_name ;and sudo chmod -R g+w /var/www/$app_name ;and sudo ln -sf /var/www/$app_name/app/$jar /var/www/$app_name/app/$app_name.jar"
    scp target/$jar jcodev.eu:/var/www/$app_name/app && \
    ssh -t jcodev.eu "sudo supervisorctl restart $app_name ;and sudo service nginx restart"
